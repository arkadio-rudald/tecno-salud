package app.entity;

import app.entity.Administrador;
import app.entity.Medicos;
import app.entity.Pacientes;
import app.entity.PersonalAdministrativo;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-06-14T13:33:57")
@StaticMetamodel(Roles.class)
public class Roles_ { 

    public static volatile SingularAttribute<Roles, String> tipoRol;
    public static volatile CollectionAttribute<Roles, PersonalAdministrativo> personalAdministrativoCollection;
    public static volatile CollectionAttribute<Roles, Pacientes> pacientesCollection;
    public static volatile CollectionAttribute<Roles, Medicos> medicosCollection;
    public static volatile CollectionAttribute<Roles, Administrador> administradorCollection;
    public static volatile SingularAttribute<Roles, Integer> idRol;

}