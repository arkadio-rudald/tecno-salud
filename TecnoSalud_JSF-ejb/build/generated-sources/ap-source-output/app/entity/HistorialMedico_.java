package app.entity;

import app.entity.Especialidad;
import app.entity.Hora;
import app.entity.Medicos;
import app.entity.Pacientes;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-06-14T13:33:57")
@StaticMetamodel(HistorialMedico.class)
public class HistorialMedico_ { 

    public static volatile SingularAttribute<HistorialMedico, Medicos> idMedicos;
    public static volatile SingularAttribute<HistorialMedico, Date> fecha;
    public static volatile SingularAttribute<HistorialMedico, Pacientes> nuhsaPaciente;
    public static volatile SingularAttribute<HistorialMedico, String> descripcion;
    public static volatile SingularAttribute<HistorialMedico, Integer> idHistorial;
    public static volatile SingularAttribute<HistorialMedico, Especialidad> idEspecialidad;
    public static volatile SingularAttribute<HistorialMedico, Hora> idHora;

}