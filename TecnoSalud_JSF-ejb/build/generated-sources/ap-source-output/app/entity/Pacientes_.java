package app.entity;

import app.entity.Citas;
import app.entity.Formulario;
import app.entity.HistorialMedico;
import app.entity.PeticionCita;
import app.entity.Roles;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-06-14T13:33:57")
@StaticMetamodel(Pacientes.class)
public class Pacientes_ { 

    public static volatile SingularAttribute<Pacientes, String> nombre;
    public static volatile SingularAttribute<Pacientes, String> direccion;
    public static volatile CollectionAttribute<Pacientes, PeticionCita> peticionCitaCollection;
    public static volatile SingularAttribute<Pacientes, String> apellidos;
    public static volatile CollectionAttribute<Pacientes, Citas> citasCollection;
    public static volatile CollectionAttribute<Pacientes, HistorialMedico> historialMedicoCollection;
    public static volatile CollectionAttribute<Pacientes, Formulario> formularioCollection;
    public static volatile SingularAttribute<Pacientes, String> contrasena;
    public static volatile SingularAttribute<Pacientes, String> dni;
    public static volatile SingularAttribute<Pacientes, Date> fechaNacimiento;
    public static volatile SingularAttribute<Pacientes, Roles> idRol;
    public static volatile SingularAttribute<Pacientes, Integer> nuhsa;

}