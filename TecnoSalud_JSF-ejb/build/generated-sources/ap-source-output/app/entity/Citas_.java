package app.entity;

import app.entity.Especialidad;
import app.entity.Hora;
import app.entity.Medicos;
import app.entity.Pacientes;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-06-14T13:33:57")
@StaticMetamodel(Citas.class)
public class Citas_ { 

    public static volatile SingularAttribute<Citas, Medicos> idMedicos;
    public static volatile SingularAttribute<Citas, Pacientes> nushaPaciente;
    public static volatile SingularAttribute<Citas, Date> fecha;
    public static volatile SingularAttribute<Citas, String> localizacion;
    public static volatile SingularAttribute<Citas, Integer> idCitas;
    public static volatile SingularAttribute<Citas, Especialidad> idEspecialidad;
    public static volatile SingularAttribute<Citas, Hora> idHora;

}