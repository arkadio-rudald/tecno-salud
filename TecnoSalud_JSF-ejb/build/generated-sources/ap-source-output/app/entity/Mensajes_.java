package app.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-06-18T15:16:55")
@StaticMetamodel(Mensajes.class)
public class Mensajes_ { 

    public static volatile SingularAttribute<Mensajes, String> hora;
    public static volatile SingularAttribute<Mensajes, String> estado;
    public static volatile SingularAttribute<Mensajes, Date> fecha;
    public static volatile SingularAttribute<Mensajes, String> contenido;
    public static volatile SingularAttribute<Mensajes, String> tipoMensaje;
    public static volatile SingularAttribute<Mensajes, Integer> idMensajes;
    public static volatile SingularAttribute<Mensajes, String> destinatario;
    public static volatile SingularAttribute<Mensajes, String> remitente;

}