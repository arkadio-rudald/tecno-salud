package app.entity;

import app.entity.Hora;
import app.entity.Pacientes;
import app.entity.TipoCita;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-06-14T13:33:57")
@StaticMetamodel(PeticionCita.class)
public class PeticionCita_ { 

    public static volatile SingularAttribute<PeticionCita, Date> fecha;
    public static volatile SingularAttribute<PeticionCita, Pacientes> nuhsaPaciente;
    public static volatile SingularAttribute<PeticionCita, Integer> idPeticion;
    public static volatile SingularAttribute<PeticionCita, TipoCita> idTipo;
    public static volatile SingularAttribute<PeticionCita, String> comentario;
    public static volatile SingularAttribute<PeticionCita, Hora> idHora;

}