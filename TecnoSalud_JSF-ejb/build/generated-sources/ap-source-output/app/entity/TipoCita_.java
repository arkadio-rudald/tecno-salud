package app.entity;

import app.entity.PeticionCita;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-06-14T13:33:57")
@StaticMetamodel(TipoCita.class)
public class TipoCita_ { 

    public static volatile SingularAttribute<TipoCita, String> nombre;
    public static volatile CollectionAttribute<TipoCita, PeticionCita> peticionCitaCollection;
    public static volatile SingularAttribute<TipoCita, Integer> idTipo;

}