package app.entity;

import app.entity.Citas;
import app.entity.Especialidad;
import app.entity.HistorialMedico;
import app.entity.Roles;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-06-14T13:33:57")
@StaticMetamodel(Medicos.class)
public class Medicos_ { 

    public static volatile SingularAttribute<Medicos, String> nombre;
    public static volatile SingularAttribute<Medicos, Integer> idMedicos;
    public static volatile SingularAttribute<Medicos, String> apellidos;
    public static volatile CollectionAttribute<Medicos, Citas> citasCollection;
    public static volatile CollectionAttribute<Medicos, HistorialMedico> historialMedicoCollection;
    public static volatile SingularAttribute<Medicos, String> contrasena;
    public static volatile SingularAttribute<Medicos, String> dni;
    public static volatile SingularAttribute<Medicos, Roles> idRol;
    public static volatile SingularAttribute<Medicos, Especialidad> idEspecialidad;

}