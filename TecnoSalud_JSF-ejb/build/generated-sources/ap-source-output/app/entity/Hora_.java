package app.entity;

import app.entity.Citas;
import app.entity.HistorialMedico;
import app.entity.PeticionCita;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-06-14T13:33:57")
@StaticMetamodel(Hora.class)
public class Hora_ { 

    public static volatile SingularAttribute<Hora, Date> hora;
    public static volatile CollectionAttribute<Hora, PeticionCita> peticionCitaCollection;
    public static volatile SingularAttribute<Hora, String> disponibilidad;
    public static volatile CollectionAttribute<Hora, Citas> citasCollection;
    public static volatile CollectionAttribute<Hora, HistorialMedico> historialMedicoCollection;
    public static volatile SingularAttribute<Hora, Integer> idHora;

}