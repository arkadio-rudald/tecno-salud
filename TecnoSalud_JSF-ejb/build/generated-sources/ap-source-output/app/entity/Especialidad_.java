package app.entity;

import app.entity.Citas;
import app.entity.HistorialMedico;
import app.entity.Medicos;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-06-14T13:33:57")
@StaticMetamodel(Especialidad.class)
public class Especialidad_ { 

    public static volatile SingularAttribute<Especialidad, String> nombre;
    public static volatile CollectionAttribute<Especialidad, Citas> citasCollection;
    public static volatile SingularAttribute<Especialidad, String> descripcion;
    public static volatile CollectionAttribute<Especialidad, HistorialMedico> historialMedicoCollection;
    public static volatile CollectionAttribute<Especialidad, Medicos> medicosCollection;
    public static volatile SingularAttribute<Especialidad, Integer> idEspecialidad;

}