package app.entity;

import app.entity.Pacientes;
import app.entity.TipoFormulario;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-06-14T13:33:57")
@StaticMetamodel(Formulario.class)
public class Formulario_ { 

    public static volatile SingularAttribute<Formulario, Date> fecha;
    public static volatile SingularAttribute<Formulario, Pacientes> nuhsaPaciente;
    public static volatile SingularAttribute<Formulario, Integer> idFormulario;
    public static volatile SingularAttribute<Formulario, TipoFormulario> idTipoform;
    public static volatile SingularAttribute<Formulario, String> comentario;

}