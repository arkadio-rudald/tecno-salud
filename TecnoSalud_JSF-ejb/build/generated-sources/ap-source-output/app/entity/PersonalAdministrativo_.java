package app.entity;

import app.entity.Roles;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-06-14T13:33:57")
@StaticMetamodel(PersonalAdministrativo.class)
public class PersonalAdministrativo_ { 

    public static volatile SingularAttribute<PersonalAdministrativo, String> nombre;
    public static volatile SingularAttribute<PersonalAdministrativo, String> apellidos;
    public static volatile SingularAttribute<PersonalAdministrativo, String> email;
    public static volatile SingularAttribute<PersonalAdministrativo, Integer> idPersonalAdmin;
    public static volatile SingularAttribute<PersonalAdministrativo, String> contrasena;
    public static volatile SingularAttribute<PersonalAdministrativo, String> dni;
    public static volatile SingularAttribute<PersonalAdministrativo, Roles> idRol;

}